VENDOR_PATH := device/xiaomi/sirius/anx-camera

PRODUCT_COPY_FILES += \
	$(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/etc,system/etc) \
	$(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm,system/priv-app/ANXCamera/lib/arm) \
	$(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/priv-app/ANXCamera/lib/arm64,system/priv-app/ANXCamera/lib/arm64) \
	$(call find-copy-subdir-files,*,$(VENDOR_PATH)/system/priv-app/ANXExtraPhoto/lib/arm64,system/priv-app/ANXExtraPhoto/lib/arm64)

PRODUCT_PACKAGES += \
    ANXCamera \
    ANXExtraPhoto
